# Backend de la aplicacion kanban hecho en laravel y mysql
Primero debemos crear la base de datos en mysql

## Available Scripts

Para levantar nuestro proyecto necesitamos modificar el .env con los datos de la base de datos, luego:

### `composer install`

### `php artisan migrate --seed`

### `php artisan serve`

esto corre http://localhost:8000

Luego ir al repositorio del front y ejecutar el cliente.

https://gitlab.com/acner/kanban-react-frontend
