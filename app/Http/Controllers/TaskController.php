<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return response()->json(['tasks' => $tasks]);
    }

    public function show($id)
    {
        $task = Task::findOrFail($id);
        return response()->json(['task' => $task]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'nullable',
            'assigned_to' => 'nullable',
            'status' => 'in:Pendiente,En Proceso,Finalizado'
        ]);

        $task = Task::create($request->all());
        $task = Task::findOrFail($task->id);
        return response()->json(['task' => $task], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'status' => 'in:Pendiente,En Proceso,Finalizado'
        ]);

        $task = Task::findOrFail($id);
        $task->update($request->all());
        return response()->json(['task' => $task]);
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return response()->json(['message' => 'Task deleted successfully']);
    }
}
